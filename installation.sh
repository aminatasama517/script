#!/bin/bash

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

sudo apt update

sudo apt install terraform

# installation nginx
sudo apt update -y

sudo apt install nginx -y

sudo nginx -v

sudo systemctl status nginx

sudo systemctl enable nginx

curl -fsSl http://localhost



